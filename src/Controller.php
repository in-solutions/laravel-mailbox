<?php

namespace Insolutions\Mailbox;
 
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Insolutions\Files\File as FileEntry;
use Carbon\Carbon;

class Controller extends \App\Http\Controllers\Controller
{

	public function mailgunStoreWebhook(Request $r) {

		$re = new ReceivedEmail;
		$re->subject = $r->subject;
		$re->from = $r->From;
		$re->to = $r->To;
		$re->data = json_encode($r->all());
		$re->data_type = 'mailgun::inbound';
		$re->save();		

		$response = self::proceedMailgunMail($re);

		return response()->json($response);
	}

	public static function proceedMailgunMail(ReceivedEmail $re) {
		if ($re->proceeded_at) { // was proceeded
			return;
		}

		$mailgun_user = 'api';
		$mailgun_secret = config('services.mailgun.secret');
		$storagePath = 'mailbox';

		$response = [];

		$r = json_decode($re->data);

		$response['recipient']  = $r->recipient;
		$response['headers'] = json_decode($r->{'message-headers'});

		foreach (json_decode($r->attachments) as $att) {
			$auth_url = str_replace(
					"https://", 
					"https://" . $mailgun_user . ':' . $mailgun_secret . '@', 
					$att->url);

			$contents = file_get_contents($auth_url);
			$contentType = $att->{'content-type'};			
			$fileName = uniqid('', true) . '_' . $att->name;

			Storage::put($storagePath . '/' . $fileName, $contents);

			$file = FileEntry::create([
				'title' => $att->name, // original file name
				'path' => $storagePath,
				'file_name' => $fileName,
				'mime_type' => $contentType,
			]);

			$response['files'][] = $file;
		} 

		$re->proceeded_at = Carbon::now();
		$re->save();

		return $response;
	}

}