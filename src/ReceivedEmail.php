<?php

namespace Insolutions\Mailbox;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceivedEmail extends Model
{
	use SoftDeletes;

    protected $table = 't_received_email';

    protected $fillable = [];

    protected $hidden = [];


}