<?php

use Insolutions\Mailbox\Controller;

Route::group(['prefix' => 'mailbox'], function () {
	
	Route::post('mailgun/receive', Controller::class . '@mailgunStoreWebhook');

});